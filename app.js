const searchBtn = document.querySelector("#ip-search");

searchBtn.addEventListener('click', async function getIp() {
  try {
    let response = await fetch('https://api.ipify.org/?format=json', {
      method: 'get'
    })
    let data = await response.json();
    let adress = data.ip;
    let query = await fetch(`${'http://ip-api.com/json/'}${adress}`, {
      method: 'get'
    })
    let ipResponse = await query.json();
    const ipField =  new Adress(ipResponse.country, ipResponse.regionName, ipResponse.city, ipResponse.region);
    ipField.render("#root");
  } catch(e) {
    alert(e);
  }
})

class Adress {
  constructor(country, region, city, regionNumber, card) {
    this.country = country;
    this.region = region;
    this.city = city;
    this.regionNumber = regionNumber;
    this.card = null;
  }
  render(selector) {
    this.card = document.createElement("span");
    this.card = `Згідно IP ваша адреса ${this.country}, ${this.regionNumber} ${this.region}, ${this.city}`;
    document.querySelector(selector).innerHTML = this.card; 
  }

}